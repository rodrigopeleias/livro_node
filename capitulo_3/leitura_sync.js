var fs = require('fs');
var leituraAsync = function(arquivo) {
	console.log("Fazendo leitura sincrona");
	var inicio = new Date().getTime();
	fs.readFileSync(arquivo);
	var fim = new Date().getTime();
	console.log("Bloqueio sincrono: " +(fim - inicio)+ "ms");	
};
module.exports = leituraAsync;