var express = require('express');
var load = require('express-load');
var cookieParser = require('cookie-parser')
var expressSession = require('express-session');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cfg = require('./config.json');
var error = require('./middleware/error');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var redis = require('./lib/redis_connect');
var ExpressStore = redis.getExpressStore();


var cookie = cookieParser(cfg.SECRET)

var store = new ExpressStore({client: redis.getClient(), prefix: cfg.KEY});

app.set('views', __dirname + "/views");
app.set('view engine', 'ejs');
app.use(expressSession({
    secret: cfg.SECRET,
    name: cfg.KEY,
    resave: true,
    saveUninitialized: true,
    store: store
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(methodOverride('_method'));
//app.use(app.router);
app.use(express.static(__dirname + '/public'));

io.use(function(socket, next) {
	var data = socket.request;
	cookie(data, {}, function(err) {
		var sessionID = data.signedCookies[cfg.KEY];
		store.get(sessionID, function(err, session) {
			if (err || !session) {
				return next(new Error('acesso negado'));
			} else {
				socket.handshake.session = session;
				return next();
			}
		});
	});
});

load('models')
    .then('controllers')
    .then('routes')
    .into(app);

load('sockets')
	.into(io);

app.use(error.notFound);
app.use(error.serverError);

server.listen(3000, function(){
    console.log("Ntalk no ar.");
}) 

module.exports = app;