module.exports = function(io) {

	var crypto = require('crypto')
		, redisConnect = require('../lib/redis_connect')
    	, redis = redisConnect.getClient()
    	, sockets = io.sockets;

	sockets.on('connection', function(client){
		var session = client.handshake.session
			, usuario = session.usuario;		


		redis.sadd('onlines', usuario.email, function(erro) {
			redis.smembers('onlines', function(erro, emails) {
				emails.forEach(function(email) {
					client.emit('notify-onlines', email);
					client.broadcast.emit('notify-onlines', email);
				});
			});
		});		

		client.on('join', function(sala) {
			if(sala) {
				sala = sala.replace('?', '');
			} else {
				var timestamp = new Date().toString();
				var md5 = crypto.creatHash('md5');
				sala = md5.update(timestamp).digest('hex');
			}
			session.sala = sala;
			cllient.join('sala');

			var msg = "<b>"+usuario.nome+":</b> entrou.<br>";
			redis.lpush(sala, msg, function(erro, msgs) {
				redis.lrange(sala, 0, -1, function(erro, msgs) {
					msgs.forEach(function(msg) {
						sockets.in(sala.emit('send-client', msg));
					});
				});				
			});
		});

		client.on('disconnect', function() {	
			var sala = session.sala;
			var msg = "<b>" + usuario.nome +":</b> saiu da sala.<br>";
			redis.lpush(sala, msg);
			client.broadcast.emit('notify-offline', usuario.email);
			sockets.in(sala).emit('send-client', msg);		
			client.leave(sala);			
		});

		client.on('send-server', function(msg){
			var msg = "<b>"+ usuario.nome +":</b> " + msg + "<br>";
			var data = {email: usuario.email, sala: sala};
			var sala = session.sala;			
			redis.lpush(sala, msg);			
			client.broadcast.emit('new-message', data);
			sockets.in(sala).emit('send-client', msg);			
		});
	});
}